﻿using KB_Djaburia.DbModel;
using KB_Djaburia.Helpers;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace KB_Djaburia.Pages
{
    /// <summary>
    /// Логика взаимодействия для ProductsPage.xaml
    /// </summary>
    public partial class ProductsPage : Page
    {
        DbSet<Поставка_товара> products;

        public ProductsPage()
        {
            InitializeComponent();

            PageHelper.PageName.Text = "Список товаров";

            products = PageHelper.ConnectDb.Поставка_товара;
            lvProducts.ItemsSource = products.ToList();
        }

        private void btnBack_Click(object sender, RoutedEventArgs e)
        {
            PageHelper.MainFrame.Navigate(new MainPage());
        }
    }
}
