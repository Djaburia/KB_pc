﻿using KB_Djaburia.DbModel;
using KB_Djaburia.Helpers;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace KB_Djaburia.Pages
{
    /// <summary>
    /// Логика взаимодействия для SellerPage.xaml
    /// </summary>
    public partial class SellerPage : Page
    {
        DbSet<Продавцы> sellers;

        public SellerPage()
        {
            InitializeComponent();

            PageHelper.PageName.Text = "Продавцы";

            sellers = PageHelper.ConnectDb.Продавцы;
            lvSeller.ItemsSource = sellers.ToList();
        }

        private void btnBack_Click(object sender, RoutedEventArgs e)
        {
            PageHelper.MainFrame.Navigate(new MainPage());
        }
    }
}
